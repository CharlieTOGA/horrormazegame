// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "apihorrorgameCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "CharacterStatComponent.h"

#include "Animation/AnimInstance.h"
#include "Engine/Engine.h"

#include "Net/UnrealNetwork.h"
#include "TimerManager.h"
//////////////////////////////////////////////////////////////////////////
// AapihorrorgameCharacter
AapihorrorgameCharacter::AapihorrorgameCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	//Set values of movement
	sprintSpeed = 600.0f;
	walkSpeed = 200.0f;
	GetCharacterMovement()->MaxWalkSpeed = 200.0f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
	FollowCamera->SetupAttachment(GetMesh(), "HeadSocket");
	
	// Create cameraLocation variable and set it to the follow camera then add 14 to the Y variable
	FVector CameraLocation = FollowCamera->GetComponentLocation();
	CameraLocation.Y += 14.0f;
	FollowCamera->SetWorldLocation(CameraLocation, false);

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	//** creates sub object of the stat class 
	CharacterStatComp = CreateDefaultSubobject<UCharacterStatComponent>("CharacterStatComponent");
}

//////////////////////////////////////////////////////////////////////////
// Input
void AapihorrorgameCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	//Movement bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &AapihorrorgameCharacter::Sprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &AapihorrorgameCharacter::StopSprinting);
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &AapihorrorgameCharacter::Crouching);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &AapihorrorgameCharacter::StopCrouching);
	PlayerInputComponent->BindAxis("MoveForward", this, &AapihorrorgameCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AapihorrorgameCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AapihorrorgameCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AapihorrorgameCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AapihorrorgameCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AapihorrorgameCharacter::TouchStopped);
}

//** start sprinting function
void AapihorrorgameCharacter::Sprint()
{
	if (CharacterStatComp->getStamina() > 10.0f)
	{
		GetCharacterMovement()->MaxWalkSpeed = sprintSpeed;
		bIsSprinting = true;
	}
}
//** stop sprinting function
void AapihorrorgameCharacter::StopSprinting()
{
	GetCharacterMovement()->MaxWalkSpeed = walkSpeed;
	bIsSprinting = false;
}
//** function handles the sprinting. called in timer so is called constantly
void AapihorrorgameCharacter::handleSprinting()
{
	if (bIsSprinting)
	{
		CharacterStatComp->StaminaDec(1.0f);
		if (CharacterStatComp->getStamina() == 0.0f)
		{
			StopSprinting();
		}
	}
}
//** crouch function
void AapihorrorgameCharacter::Crouching()
{
		Crouch();
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Crouching Test"));
		NativeUpdateAnimation();
		FVector CameraLocation = FollowCamera->GetComponentLocation();
		CameraLocation.Z -= 50.f;
		FollowCamera->SetWorldLocation(CameraLocation, false);
}
//** stop crouching function
void AapihorrorgameCharacter::StopCrouching()
{
		UnCrouch();
		FVector CameraLocation = FollowCamera->GetComponentLocation();
		CameraLocation.Z += 50.f;
		FollowCamera->SetWorldLocation(CameraLocation, false);
}
//** called when the game starts
void AapihorrorgameCharacter::BeginPlay()
{
	Super::BeginPlay();

	GetWorld()->GetTimerManager().SetTimer(sprintingHandle, this, &AapihorrorgameCharacter::handleSprinting, 0.1f, true);
}

//** function updates animation
void AapihorrorgameCharacter::NativeUpdateAnimation()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Anim Update"));
	if (!owner)
	{
		return;

		if (owner->IsA(AapihorrorgameCharacter::StaticClass()))
		{
			AapihorrorgameCharacter* PlayerCharacter = Cast<AapihorrorgameCharacter>(owner);
			if (PlayerCharacter)
			{
				bIsCrouching = PlayerCharacter->GetCharacterMovement()->IsCrouching();
			}
		}
	}
}
//** function converts stamina in to an FString that can be printed to the screen in BP's
FString AapihorrorgameCharacter::returnCharacterStats()
{
	FString RetString = "Stamina " + FString::SanitizeFloat(CharacterStatComp->getStamina());
		return RetString;
		RetString = NULL;
}

void AapihorrorgameCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	Jump();
}

void AapihorrorgameCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	StopJumping();
}

void AapihorrorgameCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AapihorrorgameCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AapihorrorgameCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AapihorrorgameCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}