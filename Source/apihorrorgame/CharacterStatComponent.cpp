// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterStatComponent.h"
#include "Net/UnrealNetwork.h"
#include "TimerManager.h"
#include "Engine/Engine.h"	
#include "UnrealNetwork.h"
	
	//** Sets default values for this component's properties
	UCharacterStatComponent::UCharacterStatComponent()
	{
		// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
		// off to improve performance if you don't need them.
		PrimaryComponentTick.bCanEverTick = true;
		stamina = 80.0f;
	}

	//** Called when the game starts
	void UCharacterStatComponent::BeginPlay()
	{
		Super::BeginPlay();

		//setting timer for handling stamina
		GetWorld()->GetTimerManager().SetTimer(StaminaHandle, this, &UCharacterStatComponent::StaminaInc, 0.2f, true);

	}
	//**stamina increase function
	void UCharacterStatComponent::StaminaInc()
	{
		if (stamina < 100.0f)
		{
			++stamina;
		}
	}
	//** stamina decrease function
	void UCharacterStatComponent::StaminaDec(float Value)
	{
		if (stamina > 0)
		{
			stamina -= Value;
		}
	}
	//** gets stamina
	float UCharacterStatComponent::getStamina()
	{
		return stamina;
	}

	//** Called every frame
	void UCharacterStatComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
	{
		Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	}