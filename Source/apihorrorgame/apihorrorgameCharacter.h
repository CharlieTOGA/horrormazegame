// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "apihorrorgameCharacter.generated.h"

UCLASS(config=Game)
class AapihorrorgameCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	//Camera variables
	/* Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;
	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	//Character movement variables
	//** Sprint speed variable
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "character Movement")
		float sprintSpeed;
	//** Walk speed vatiable
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "character Movement")
		float walkSpeed;
	////** Stamina variable
	//** Crouch variable
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "character Movement")
		float crouchVar;
	//** Boolean isCrouching check
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "character Movement")
		bool bIsCrouching;
	//** Boolean isMoving check
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "character Movement")
		bool bIsSprinting;

	//** sprinting timer handle
	FTimerHandle sprintingHandle;

public:
	//** pointer to the stat component class
	UPROPERTY(EditAnywhere)
	class UCharacterStatComponent* CharacterStatComp;

	//** prints stats of character on screen
	UFUNCTION(BlueprintPure)
	FString returnCharacterStats();

protected:
	//Movement Functions
	void Sprint();
	void StopSprinting();
	void handleSprinting();
	
	void Crouching();
	void StopCrouching();
	
	//** called when the game starts
	virtual void BeginPlay() override;


	/* Called for forwards/backward & side to side input */
	void MoveForward(float Value);
	void MoveRight(float Value);

	/* Called via input to turn at a given "RATE" (this is what rate is used for!).
	 @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate */
	void TurnAtRate(float Rate);
	void LookUpAtRate(float Rate);

	/* Handler for when a touch input begins/stops */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);


	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	AapihorrorgameCharacter();
	
	//virtual function for updating animations
	virtual void NativeUpdateAnimation();

	//follow camera stuff
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

private:
	//pointer owner
	APawn* owner;
};