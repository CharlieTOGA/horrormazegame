// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CharacterStatComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class APIHORRORGAME_API UCharacterStatComponent : public UActorComponent
{
	GENERATED_BODY()

protected:
		//variables
		//**stamina variable
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "character Movement")
		float stamina;

	//stamina timer
	FTimerHandle StaminaHandle;

public:	
	//functions
	UCharacterStatComponent();
	void StaminaInc();
	void StaminaDec(float Value);
	float getStamina();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;	
};
