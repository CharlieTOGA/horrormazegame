// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "apihorrorgameGameMode.generated.h"

UCLASS(minimalapi)
class AapihorrorgameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AapihorrorgameGameMode();
};



